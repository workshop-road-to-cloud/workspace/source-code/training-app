# TrainingAppWeb

## In Vagrant
1.- Install NodeJS
```
sudo apt install nodejs
sudo apt install -y nodejs
```

2.- Install NPM
```
sudo apt install -y npm
```

## Prepare environment

1.- Install JSon Server
```
npm install -g json-server
```

2.- Start the Mock Server. Run this code in the same folder as `db.json` and `routes.json`
```
json-server --watch db.json --routes routes.json --delay 1000
```

The `--delay` command set the response time for each request (in millis)
