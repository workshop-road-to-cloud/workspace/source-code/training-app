import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { NoteListComponent } from './components/note-list/note-list.component';
import { NoteDetailComponent } from './components/note-detail/note-detail.component';
import { NoteOptionsComponent } from './components/note-options/note-options.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { NoteService } from './commons/services/note.service';
import { AuthService } from './commons/utils/auth.service';
import { HttpClientService } from './commons/utils/http-client.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './app.routes';
import { HomeComponent } from './components/home/home.component';
import { NoteResolver } from './commons/resolver/note.resolver';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NoteListComponent,
    NoteDetailComponent,
    NoteOptionsComponent,
    SpinnerComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(APP_ROUTES, { useHash: false }),
  ],

  //=========== Services ===========
  providers: [
    AuthService, HttpClientService, NoteService, NoteResolver
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
