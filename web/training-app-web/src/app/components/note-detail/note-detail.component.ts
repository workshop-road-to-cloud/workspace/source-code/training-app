import { Component, OnInit } from '@angular/core';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Note } from 'src/app/commons/model/note.model';
import { NoteService } from 'src/app/commons/services/note.service';

@Component({
  selector: 'training-app-note-detail',
  templateUrl: './note-detail.component.html',
  styleUrls: ['./note-detail.component.scss']
})
export class NoteDetailComponent implements OnInit {

  private notesObservable: any;
  protected noteId: number | undefined;
  protected note: Note | null = null;
  protected htmlBody: SafeHtml | undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private noteService: NoteService,
    private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    //Obtaining Note ID from Resolver
    this.notesObservable = this.route.params.subscribe(() => {

      this.noteId = this.route.snapshot.data['noteId'];
      console.info("Finding Note with Id=", this.noteId);

      if(this.noteId !== undefined) {
        this.note = null;

        this.noteService.findNoteDetail(this.noteId!)
          .then(response => {
            this.note = response as Note;
            this.htmlBody = this.sanitizer.bypassSecurityTrustHtml(this.note.body);
          });
      }
    });
  }
}
