import { Component } from '@angular/core';
import { Note } from 'src/app/commons/model/note.model';
import { NoteService } from 'src/app/commons/services/note.service';

@Component({
  selector: 'training-app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent {

  protected searchingNotes = false;
  protected notes: Note[] | null = null;

  constructor(private noteService: NoteService){

    this.searchingNotes = true;

    this.noteService.searchAllNotes()
      .then(response => {
        this.notes = response as Note[];
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.searchingNotes = false;
      });
  }
}
