import { provideRoutes } from "@angular/router";
import { NoteResolver } from "./commons/resolver/note.resolver";
import { GeneralConstants } from "./commons/utils/general.constants";
import { HomeComponent } from "./components/home/home.component";
import { NoteDetailComponent } from "./components/note-detail/note-detail.component";

export const APP_ROUTES = [

  /**
   * Always execute the parent resolver, when navigate through the web only the children resolvers
   * will be executed.
   * If try to load directly a children path (using browser's location bar) then the parent
   * and the children resolvers will be executed.
   */
  {
    path: GeneralConstants.ROUTES.HOME,
    resolve: {
    },
    children: [
      {
        path: GeneralConstants.ROUTES.HOME,
        component: HomeComponent
      },
      {
        path: GeneralConstants.ROUTES.NOTE_DETAIL + "/:id",
        component: NoteDetailComponent,
        resolve: {
          noteId: NoteResolver
        }
      },
      {
        //If the user select and invalid route the redirect to Home
        path: GeneralConstants.ROUTES.INVALID,
        component: HomeComponent,
      }
    ]
  }
];

export const APP_ROUTES_PROVIDER = [
  provideRoutes(APP_ROUTES)
];
