import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";

/**
 * Obtain the query param "id" and pass it to the next view
 */
@Injectable()
export class NoteResolver implements Resolve<number> {

  constructor() {}

  resolve(route: ActivatedRouteSnapshot) {
    let noteId = this.getAsNumber(route.paramMap.get('id'));
    console.log("In NoteResolver, noteId=", noteId);
    return noteId;
  }

  getAsNumber(text: string | null) {
    return +text!;
  }
}
