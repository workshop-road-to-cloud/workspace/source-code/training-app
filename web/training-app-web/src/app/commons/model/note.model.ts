export class Note {
    constructor(
        public id: string,
        public title: string,
        public bodyMinimal: string,
        public body: string,
        public lastUpdatedDate: number) {
    }
}