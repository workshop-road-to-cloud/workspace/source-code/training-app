import { GeneralConstants } from "../utils/general.constants";

export class ErrorResponse {

  public body: any;
  public status: number;

  constructor(error: any) {
    this.status = error["status"]

    if (this.status != GeneralConstants.HTTP_STATUS.BACKEND_NOT_AVAILABLE) {
      this.body = JSON.parse(error["_body"]);
    } else {
        console.error("Unparsed error:", this.body);
    }
  }
}
