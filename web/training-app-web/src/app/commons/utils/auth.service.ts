import { Injectable } from "@angular/core";

@Injectable()
export class AuthService {

    obtainTokenPromise(): Promise<any> {
        //Until now there isn't an Authentication service
        return new VoidPromise((resolve, reject) => {
            resolve();
        });
    }
}

class VoidPromise extends Promise<void>{
    constructor(callback: (resolve: () => void, reject: (error: any) => void) => void) {
        super(callback);
    }
}
