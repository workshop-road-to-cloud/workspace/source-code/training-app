import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Subscription } from 'rxjs';
import { map, retry, timeout } from 'rxjs/operators';
import { ErrorResponse } from "../model/error-response.model";
import { GeneralConstants } from "../utils/general.constants";
import { AuthService } from "./auth.service";


/**
 * This Service doesn't require a AuthService injection.
 * It was created to avoid the circular 
 * reference
 */
@Injectable()
export class HttpClientService {

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService) {
  }

  /**
   * Execute GET request.
   *
   * @param url
   * @param params
   * @returns {Promise<T>}
   */
  get(
    url: string,
    params: HttpParams | undefined) {
    var getPromise = new Promise((resolve, reject) => {
      this.authService.obtainTokenPromise()
      .then((token) => {
        let currentSubscription: Subscription = this.httpClient
          .get(url, {
            params: params,
            responseType: 'json' as const,
            observe: 'response',
          })
          .pipe(
            map((response) => {
              console.info(`Response of [GET]${url}:`, response.body);
              return response.body;
            }),
            timeout({ each: GeneralConstants.CONFIGURATION.TIMEOUT }),
            retry({
              count: GeneralConstants.CONFIGURATION.MAX_NUM_RETRY,
              delay: GeneralConstants.CONFIGURATION.DELAY_BETWEEN_RETRY,
            })
          )
          .subscribe({
            next: (data) => resolve(data),
            error: (error) => {
              console.error('Backend error: ', error);
              reject(error);
            },
          });
        return currentSubscription;
      })
      .catch((error) => {
        reject(new ErrorResponse(error));
      });

    }).catch((err) => {
      throw err;
    });
    return getPromise;
  }
}