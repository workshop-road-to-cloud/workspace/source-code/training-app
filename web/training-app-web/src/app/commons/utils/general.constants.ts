export const GeneralConstants = {
    ROUTES: {
        HOME: "",
        INVALID: "**",
        NOTE_DETAIL: "note"
    },

    CONFIGURATION: {
        MAX_NUM_RETRY: 0,
        TIMEOUT: 10_000, //In milliseconds
        DELAY_BETWEEN_RETRY: 1000 //In milliseconds
    },

    HTTP_STATUS: {
        BACKEND_NOT_AVAILABLE: 0,
        VALIDATION: 400,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403,
        BACKEND_ERROR: 500,
        REDIRECT_TO_FULL_REGISTER: 417
    }
}