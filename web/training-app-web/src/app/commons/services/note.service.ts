import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpClientService } from "../utils/http-client.service";

@Injectable()
export class NoteService {

    constructor(private httpClientService: HttpClientService) {
    }

    searchAllNotes() {
        return this.httpClientService.get(
            environment.BACKEND_PATH + "/notes", undefined);
    }

    findNoteDetail(noteId: number) {
        return this.httpClientService.get(
            environment.BACKEND_PATH + "/notes/" + noteId, undefined);
    }
}