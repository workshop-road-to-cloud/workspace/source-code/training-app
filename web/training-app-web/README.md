# TrainingAppWeb

Follow these steps to configure the Application

## Prepare environment

1.- Install NodeJS
https://nodejs.org/en/download/

2.- Install NPM: It will able to download all the packages required to work with Angular
```
npm install -g npm@latest
```

3.- Clean NPM cache: To use the new versions instead the previously download packages
```
npm cache clean --force
```

4.- Disable NPM security audits
```
npm set audit false
```

5.- Avoid to use old angular-cli versions, we want the new one
```
npm uninstall -g angular-cli
npm uninstall -g @angular/cli
npm cache clean --force
```

6.- Install the last version on angular-cli
```
npm install -g @angular/cli@latest
```

## Install Application
In project folder if the project already exist:
```
npm install
```

## Run Application
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.


## Deploy Application

1.- Build the application, the configuration in `environment.prod.ts` will be automatically selected for the build
```
ng build
```

2.- Copy everything within the output folder `dist/training-app-web/` to NGINX folder `html`